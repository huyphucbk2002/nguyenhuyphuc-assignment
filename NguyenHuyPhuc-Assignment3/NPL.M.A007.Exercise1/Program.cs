﻿using System.Text;
using NPL.M.A007.Exercise1;
Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;

Console.WriteLine(string.Format("{0, -20}{1, -20}{2, -20}{3, -20}", "Book Name", "ISBN", "Author Name", "Publisher Name"));
Book book = new Book("Jacky roll", 12345678 ,"Smith", "Cammel");
Console.WriteLine(book.GetBookInformation());
Console.ReadKey();
