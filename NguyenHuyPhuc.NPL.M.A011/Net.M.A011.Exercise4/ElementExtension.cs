﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.M.A011.Exercise4
{
    internal static class ElementExtension
    {
        public static T SecondLargest<T>(this T[] array) where T : IComparable<T>
        {
            if (array.Length < 2)
            {
                throw new InvalidOperationException("Mảng phải có ít nhất 2 phần tử.");
            }

            return array.OrderByDescending(x => x).ElementAt(1);
        }

        public static T OrderLargest<T>(this T[] array, int order) where T : IComparable<T>
        {
            if (order < 1 || order > array.Length)
            {
                throw new ArgumentException("Thứ tự không hợp lệ.");
            }

            return array.OrderByDescending(x => x).ElementAt(order - 1);
        }
        public static T[] RemoveDuplicate<T>(this T[] array)
        {
            List<T> listBefore = array.ToList();
            List<T> listAfter = new List<T>();
            foreach (T i in listBefore)
            {
                if (listAfter.Contains(i))
                { continue; }
                else
                { listAfter.Add(i); }
            }
            return listAfter.ToArray();
        }
    }
}
