﻿


using System.Globalization;



string b = "2003/01/03 12:30:20";

Console.WriteLine(DateTime.Parse(b));
Console.WriteLine(DateTime.ParseExact(b,"yyyy/MM/dd hh:mm:ss",null));
//Console.WriteLine(DateTime.ParseExact(b, "yyyy/MM/dd", null));
Console.WriteLine(DateTime.TryParse(b, out DateTime c));
Console.WriteLine(DateTime.TryParseExact(b, "yyyy/MM/dd hh:mm:ss",null,DateTimeStyles.None,out DateTime d));
