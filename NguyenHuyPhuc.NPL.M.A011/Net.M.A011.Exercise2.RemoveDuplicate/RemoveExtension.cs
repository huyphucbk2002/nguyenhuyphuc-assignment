﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.M.A011.Exercise2.RemoveDuplicate
{
    internal static class RemoveExtension
    {
        public static int[] RemoveDuplicate(this int[] array)
        {
            List<int> listNumber = array.ToList();
            List<int> listNumberAfter = new List<int>();    
            foreach(int i in listNumber)
            {
                if (listNumberAfter.Contains(i))
                { continue; }
                else
                { listNumberAfter.Add(i); }
            }
            return listNumberAfter.ToArray();
        }
        
        public static T[] RemoveDuplicate<T>(this T[] array)
        {
            List<T> listBefore = array.ToList();
            List<T> listAfter = new List<T>();
            foreach (T i in listBefore)
            {
                if (listAfter.Contains(i))
                { continue; }
                else
                { listAfter.Add(i); }
            }
            return listAfter.ToArray();
        }
    }
}
