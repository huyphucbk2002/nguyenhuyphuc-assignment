﻿using Net.M.A011.Exercise2.RemoveDuplicate;

int[] array = new int[] { 1, 2, 3, 3, 5, 6, 8 };
array = array.RemoveDuplicate();
foreach(int i in array)
{
    Console.Write(i + " ");
}    
Console.WriteLine();

string[] name = new string[] {"Vu", "Van", "Hau", "Dat", "Huy", "Vu" };
name = name.RemoveDuplicate<String>();
foreach(string s in name)
{
    Console.Write(s + " ");
}    
