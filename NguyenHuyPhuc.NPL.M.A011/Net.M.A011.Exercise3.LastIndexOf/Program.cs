﻿using Net.M.A011.Exercise3.LastIndexOf;

int[] number = new int[] { 1,2,3,5,7,3,2};
int i = number.LastIndexOf<int>(3);
Console.WriteLine(i);

string[] name = new string[] { "Hoang", "Trong", "Hieu", "A", "Hoang", "Trong" };
int i1 = name.LastIndexOf<string>("Trong");
Console.WriteLine(i1);