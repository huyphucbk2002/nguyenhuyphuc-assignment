﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Net.M.A011.Exercise1.ArrayListGenericExtension
{
    internal static class ExtensionMethod
    {
        public static int CountInt(this ArrayList array)
        {
            return array.OfType<int>().Count();
        }

        public static int CountOf(this ArrayList array, Type dataType)
        {
            return array.Cast<object>().Count(item => dataType.IsInstanceOfType(item));
        }

        public static int CountOf<T>(this ArrayList array)
        {
            return array.OfType<T>().Count();
        }

        public static T MaxOf<T>(this ArrayList array) where T : IComparable
        {
            var numericItems = array.OfType<T>();
            if (numericItems.Any())
            {
                return numericItems.Max();
            }
            throw new InvalidOperationException("Không tìm thấy phần tử có kiểu số cụ thể trong ArrayList.");
        }

    }
}
