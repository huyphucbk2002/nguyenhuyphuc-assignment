﻿using Net.M.A011.Exercise1.ArrayListGenericExtension;
using System.Collections;
ArrayList input = new ArrayList();
input.Add("Hau");
input.Add("Van");
input.Add(1);
input.Add(2);
input.Add("Nguyen");
input.Add(5.9d);

Console.WriteLine(input.CountInt());
Console.WriteLine(input.CountOf(typeof(int)));
Console.WriteLine(input.CountOf<string>());
Console.WriteLine(input.MaxOf<int>());
