﻿//ConsoleKeyInfo là một lớp trong C# được sử dụng để đại diện cho thông tin về một phím được nhấn
//hoặc đã thả trong một ứng dụng console.

//Key: enum ConsoleKey xác định kiểu phím như Enter, Backspace, A, B, C, và nhiều kiểu phím khác.
//KeyChar: Đây là ký tự tương ứng với phím được nhấn. Nếu phím không phải là một ký tự (ví dụ: Enter, Esc), thì KeyChar sẽ có giá trị '\0'.
//Modifiers: Xác định xem các phím Control, Alt, Shift, hoặc Windows đã được nhấn kèm theo phím chính.
//ConsoleKeyInfo thường được sử dụng trong các ứng dụng console để xử lý sự kiện nhập từ bàn phím và
//cho phép kiểm tra xem người dùng đã nhấn phím nào và có kèm theo các phím điều khiển nào
//(Ctrl, Alt, Shift) để thực hiện các tác vụ tương ứng.

//using System.Collections;
//using System.Text.RegularExpressions;

using System.Text.RegularExpressions;

class ConvertDateTime
{
    //Hàm checkInput cho phép người dùng nhập dữ liệu từng hàng đến khi ấn CTRL+Enter để kết thúc
    public static List<string> InputRecord()
    {

        List<string> records = new List<string>();

        while (true)
        {
            ConsoleKeyInfo press = Console.ReadKey();

            if (press.Key == ConsoleKey.Enter && press.Modifiers == ConsoleModifiers.Control)
            {
                break;
            }
            string record = press.KeyChar + Console.ReadLine();
            records.Add(record);
        }
        return records;
    }

    //Hàm CheckFormatRecord để kiểm tra các dòng nhập vào có đúng format không
    public static bool CheckFormatRecord(List<string> records)
    {
        string pattern = @"[a-zA-Z\s]+ at (\d{2}/\d{2}/\d{4} \d{2}:\d{2}:\d{2} [APap][Mm] by [A-Za-z]+)$";
        foreach (string record in records)
        {
            if (!Regex.IsMatch(record, pattern))
            {
                return false;
            }
        }
        return true;
    }

    //Hàm GetDateTimeString để lấy ra thời gian từng dòng ghi vào dưới dạng string
    public static List<string> GetDateTimeString(List<string> records)
    {
        List<string> dates = new List<string>();
        string pattern = @"\d{2}/\d{2}/\d{4} \d{2}:\d{2}:\d{2} [APap][Mm]";
        foreach (string record in records)
        {
            if (Regex.IsMatch(record, pattern))
            {
                dates.Add(Regex.Match(record, pattern).Value);
            }
        }
        return dates;
    }

    //Hàm ParseDateTime dùng để chuyển thời gian trong các dòng từ string sang DateTime
    public static List<DateTime> ParseDateTime(List<string> dates)
    {
        List<DateTime> dateTimes = new List<DateTime>();
        try
        {
            foreach (string date in dates)
            {
                DateTime parseDate = DateTime.ParseExact(date, "dd/MM/yyyy hh:mm:ss tt", null);
                dateTimes.Add(parseDate);
            }
            return dateTimes;
        }
        catch (Exception)
        {
            return dateTimes;
        }
    }

    //Hàm SortDateTime dùng để sắp xếp các mốc thời gian theo thứ tự tăng dần
    public static List<DateTime> SortDateTime(List<DateTime> dates)
    {
        DateTime temp;
        for (int i = 0; i < dates.Count - 1; i++)
        {
            for (int j = 0; j < dates.Count - i - 1; j++)
            {
                if (dates[j] > dates[j + 1])
                {
                    temp = dates[j];
                    dates[j] = dates[j + 1];
                    dates[j + 1] = temp;
                }
            }
        }
        return dates;
    }

    //Hàm SortedRecord để in ra kết quả các record người dùng nhập vào theo thứ tự thời gian tăng dần

    public static List<string> SortedRecord(List<string> records, List<DateTime> dates)
    {
        List<string> sortedRecord = new List<string>();
        foreach (DateTime date in dates)
        {
            foreach (string record in records)
            {
                if (record.Contains(date.ToString("dd/MM/yyyy hh:mm:ss tt").ToUpper()))
                {
                    sortedRecord.Add(record);
                }
            }
        }
        return sortedRecord;
    }

    public static void Main(string[] args)
    {
        Console.WriteLine("Input create and update information: ");
        List<string> records = InputRecord();
        bool check = CheckFormatRecord(records);
        if (check)
        {
            List<string> dateString = GetDateTimeString(records);
            List<DateTime> dates = ParseDateTime(dateString);
            if (dates != null)
            {
                dates = SortDateTime(dates);
                records = SortedRecord(records, dates);
                Console.WriteLine("Create and update information after sorting time ascending:");
                foreach (string record in records)
                {
                    Console.WriteLine(record);
                }
            }
            else
            {
                Console.WriteLine("Invalid Format Date!");
            }

        }
        else
        {
            Console.WriteLine("Invalid Format Record!");
        }

    }
}


