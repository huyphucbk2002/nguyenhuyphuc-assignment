﻿class Normalize
{
    static void Main(string[] args)
    {
        Console.Write("Enter your name: ");
        string name = Console.ReadLine();
        name = NormalizeName(name);
        Console.Write("Name after being normalized: " + name);
        Console.ReadLine();
    }
    public static string NormalizeName(string name)
    {
        
        string[] nameElement = name.Split(' ', StringSplitOptions.RemoveEmptyEntries);
        for (int i = 0; i < nameElement.Length; i++)
        {
            if (nameElement[i].Length > 1)
            {
                string firstLetter = nameElement[i].Substring(0, 1).ToUpper();
                string anotherLetter = nameElement[i].Substring(1).ToLower();
                nameElement[i] = firstLetter + anotherLetter;
            }
            else if (nameElement[i].Length == 1)
                nameElement[i] = nameElement[i].ToUpper();
        }
        return string.Join(" ", nameElement);
    }
}