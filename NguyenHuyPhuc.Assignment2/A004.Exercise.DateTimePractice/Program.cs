﻿using System.Globalization;
using System.Linq.Expressions;
using System.Reflection.Metadata.Ecma335;
using System.Security.Cryptography.X509Certificates;

class DatePractice
{
    //Hàm ParseDate để ép người dùng nhập vào string thời gian đúng định dạng và trả về kiểu dữ liệu datetime cho nó
    public static DateTime ParseDate()
    {
        DateTime parseDate;
        while (true)
        {
            try
            {
                Console.Write("Enter a datetime string: ");
                string date = Console.ReadLine().ToString();
                string[] formatDate = new string[] {"MM/dd/yyyy", "MM/dd/yyyy hh:mm:ss",
                                            "M/d/yyyy h:mm:ss", "M/d/yyyy hh:mm tt",
                                            "M/d/yyyy hh tt", "M/d/yyyy h:mm"};
                foreach (string format in formatDate)
                {
                    parseDate = DateTime.ParseExact(date, format, CultureInfo.InvariantCulture);
                    if (parseDate != null)
                    {
                        return parseDate;
                    }
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Invalid Format DateTime!");
            }
        }
    }
    public static String GetLastDayOfMonth(DateTime date)
    {
        DateTime a = date.AddMonths(1).AddDays(-date.Day);
        return a.ToString("dd/MM/yyyy");
    }
    public int CountWorkingDays(DateTime date)
    {
        String finishDay = GetLastDayOfMonth(date);
        int count = 0;
        DateTime finishDate = DateTime.Parse(finishDay);
        DateTime startDate = date.AddDays(-date.Day + 1);
        while (startDate.Day < finishDate.Day)
        {
            if (startDate.DayOfWeek != DayOfWeek.Sunday && startDate.DayOfWeek != DayOfWeek.Saturday)
            {
                count += 1;
            }
            startDate = startDate.AddDays(1);
        }
        if (finishDate.DayOfWeek != DayOfWeek.Sunday && finishDate.DayOfWeek != DayOfWeek.Saturday)
        {
            return count + 1;
        }
        return count;
    }

    public static void Main(string[] args)
    {
        DateTime parseDate = ParseDate();
        Console.WriteLine("Date inputed: ");
        Console.WriteLine(parseDate.ToString("dd/MM/yyyy"));
        Console.WriteLine("The last day of the month is: ");
        string lastDay = GetLastDayOfMonth(parseDate);
        Console.WriteLine(lastDay);
        Console.WriteLine("The number of working days in this month: ");
        DatePractice d = new DatePractice();
        Console.WriteLine(d.CountWorkingDays(parseDate));



    }
}