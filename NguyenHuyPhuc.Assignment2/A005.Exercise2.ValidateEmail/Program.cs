﻿using System.Text.RegularExpressions;

class ValidateEmail
{
    static bool IsEmail(string email)
    {
        string[] part = email.Split('@');
        if (part.Length != 2)
        {
            return false;
        }

        string localPart = part[0];
        string domainPart = part[1];

        //Check format local part
        if (localPart.Length == 0 || localPart[0] == '.' || localPart[localPart.Length - 1] == '.')
        {
            return false;
        }
        
        string patternLocal = @"^[\w!#$%&'*+\-\/=?^_{}|~]*(\.[\w!#$%&'*+\-\/=?^_{}|~]*)?$";
        if(!Regex.IsMatch(localPart, patternLocal))
        {
            return false;
        }

        // Check format domain part
        if (domainPart.Length == 0 || domainPart[0] == '-' || domainPart[domainPart.Length - 1] == '-')
        {
            return false;
        }

        string patternDomain = @"^[\w-]*(\.[\w-]*)*$";
        if (!Regex.IsMatch(domainPart, patternDomain))
        {
            return false;
        }

        // Check if the top-level domain is all-numeric or not 
        string[] domainParts = domainPart.Split('.');
        if (domainParts.Length >= 1 && domainParts[domainParts.Length - 1].All(char.IsDigit))
        {
            return false;
        }

        return true;
    }
    static void Main()
    {
        Console.Write("Enter an email address: ");
        string email = Console.ReadLine().Trim();
        while (!IsEmail(email))
        {
            Console.Write("Invalid email!\n Enter again: ");
            email = Console.ReadLine().Trim();
        }
        Console.ReadKey();
    }


}

