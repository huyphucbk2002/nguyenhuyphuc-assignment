﻿while (true)
{
    try
    {

        while (true)
        {
            Console.WriteLine("Enter payment invoice (dd/mm/yyyy): ");

            //Hàm DateTime.ParseExact chuyển đổi chuỗi nhập vào có định dạng "dd/MM/yyyy" thành DateTime và trả về kết quả
            DateTime inputDate = DateTime.ParseExact(Console.ReadLine(), "dd/MM/yyyy", null);

            int remiderCount = 1;
            DateTime reminder = inputDate;





            //Vòng lặp While in ra các Remind
            while (remiderCount <= 5)
            {
                if (remiderCount == 1)
                {
                    if (inputDate.DayOfWeek == DayOfWeek.Monday || inputDate.DayOfWeek == DayOfWeek.Tuesday || inputDate.DayOfWeek == DayOfWeek.Wednesday)
                    {
                        reminder = inputDate.AddDays(9);
                    }
                    else
                        reminder = inputDate.AddDays(11);
                }

                if (remiderCount == 2)
                {
                    if (reminder.DayOfWeek == DayOfWeek.Monday || reminder.DayOfWeek == DayOfWeek.Tuesday || reminder.DayOfWeek == DayOfWeek.Wednesday)
                    {
                        reminder = reminder.AddDays(2);
                    }
                    else
                        reminder = reminder.AddDays(4);

                }
                if (remiderCount == 3 || remiderCount == 4 || remiderCount == 5)
                {
                    if (reminder.DayOfWeek == DayOfWeek.Monday || reminder.DayOfWeek == DayOfWeek.Tuesday || reminder.DayOfWeek == DayOfWeek.Wednesday || reminder.DayOfWeek == DayOfWeek.Thursday)
                    {
                        reminder = reminder.AddDays(1);
                    }
                    else
                        reminder = reminder.AddDays(3);
                }
                remiderCount++;
                Console.WriteLine(remiderCount + "st reminder: " + reminder.ToString("dd/MM/yyyy"));
            }
            Console.ReadLine();
        }
    }
    catch (Exception)
    {
        Console.WriteLine("Invalid format date! Please try again!");
    }
}





