﻿using A006.Exercise.EmployeeManagement;

using System.Text;
Console.OutputEncoding = Encoding.UTF8;
Console.WriteLine("========= Assignment 06 - EmployeeManagement =========");
int choice;
Management management = new Management();
management.InitialEmployeeList();
do
{
    Console.WriteLine("Please select the admin area you require:");
    Console.WriteLine("1. Import Employee.");
    Console.WriteLine("2. Display Employees Information.");
    Console.WriteLine("3. Search Employee.");
    Console.WriteLine("4. Exit.");
    Console.Write("Enter Menu Option Number: ");
    choice = Validate.ValidateChoice(1, 4);

    switch (choice)
    {
        case 1:
            management.ImportEmployee();
            break;

        case 2:
            management.DisplayEmployee();
            break;
        case 3:
            management.SearchEmployee();
            break;
        case 4:
            Console.WriteLine("End Program!");
            break;
        default:
            Console.WriteLine("Không có lựa chọn!");
            break;
    }
}
while (choice > 0 && choice < 4);
Console.ReadKey();


