﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A006.Exercise.EmployeeManagement
{
    internal class HourlyEmployee : Employee
    {
        private double wage { get; set; }
        private double workingHour { get; set; }

        public HourlyEmployee(
            string ssn,
            string firstName,
            string lastName,
            DateTime birthDate,
            string phone,
            string email,
            double wage,
            double workingHour) : base(ssn, firstName, lastName, birthDate, phone, email)
        {
            this.wage = wage;
            this.workingHour = workingHour;
        }

        public HourlyEmployee(){
        }

        public override string? ToString()
        {
            return base.ToString() + string.Format("{0, -7} {1, -10}", wage, workingHour); 

        }
    }

}
