﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Numerics;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;

namespace A006.Exercise.EmployeeManagement
{
    internal class Management
    {
        public List<Employee> employeeList = new List<Employee>();
        public void InitialEmployeeList()
        {
            employeeList.Add(new SalariedEmployee("SE1", "Cam Tu", "Nguyễn", DateTime.Parse("02/01/2003"), "0963330122", "ntct.tu2003@gmail.com", 10, 10000, 2000));
            employeeList.Add(new SalariedEmployee("SE2", "Thị Hòa", "Ngô", DateTime.Parse("02/01/1973"), "0987747909", "nth.hoa1973@gmail.com", 10, 10000, 2000));
            employeeList.Add(new SalariedEmployee("SE3", "Cẩm Vân", "Nguyễn", DateTime.Parse("18/03/1997"), "0969979851", "ntcv.van1997@gmail.com", 10, 10000, 2000));
            employeeList.Add(new SalariedEmployee("SE4", "Thái Bảo", "Nguyễn", DateTime.Parse("12/03/2001"), "0924502239", "ntb.bao2001@gmail.com", 10, 10000, 2000));
            employeeList.Add(new SalariedEmployee("SE5", "Thái THủy", "Nguyễn", DateTime.Parse("17/09/1971"), "0934242888", "ntt.thuy1971@gmail.com", 10, 10000, 2000));

            employeeList.Add(new HourlyEmployee("HE1", "Thu Trang", "Trần", DateTime.Parse("02/04/2003"), "0394892393", "ttt.trang2003@gmail.com", 10000, 10));
            employeeList.Add(new HourlyEmployee("HE2", "Thị Thủy", "Đỗ", DateTime.Parse("23/04/1994"), "0245893333", "dtt.thuy1994@gmail.com", 10000, 10));
            employeeList.Add(new HourlyEmployee("HE3", "Hà Anh", "Bùi", DateTime.Parse("20/11/2004"), "0922284482", "bha.anh2004@gmail.com", 10000, 10));
            employeeList.Add(new HourlyEmployee("HE4", "Phương Dung", "Đỗ", DateTime.Parse("19/01/1999"), "0284524522", "dpd.dung1999@gmail.com", 10000, 10));
            employeeList.Add(new HourlyEmployee("HE5", "Quỳnh Mai", "Trần", DateTime.Parse("02/05/2000"), "0984932345", "tqm.mai2000@gmail.com", 10000, 10));
        }
        public void ImportEmployee()
        {
            Console.WriteLine("========= Import Employee =========");
            Console.WriteLine("1. Salaried Employee.");
            Console.WriteLine("2. Hourly Employee.");
            Console.WriteLine("3. Main Menu");
            Console.Write("Enter Menu Option Number: ");
            int choice = Validate.ValidateChoice(1, 3);
            if (choice == 1 || choice == 2)
            {
                Console.Write("Enter First Name: ");
                string firstName = Validate.ValidateName();
                Console.Write("Enter Last Name: ");
                string lastName = Validate.ValidateName();
                Console.Write("Enter Date Of Birth: ");
                DateTime birthDate = Validate.ValidateBirthDate();
                Console.Write("Enter Phone Number: ");
                string phone = Validate.ValidatePhoneNumber();
                Console.Write("Enter Email: ");
                string email = Validate.ValidateEmail();
                if (choice == 1)
                {
                    string ssn = AutoIncrementSSN("SE");
                    Console.Write("Enter Commission Rate: ");
                    double commissionRate = Validate.ValidateNumber("Commission Rate");
                    Console.Write("Enter Gross Sales: ");
                    double grossSales = Validate.ValidateNumber("Gross Sales");
                    Console.Write("Enter Basic Salary: ");
                    double basicSalary = Validate.ValidateNumber("Basic Salary");

                    SalariedEmployee salariedEmployee = new SalariedEmployee(ssn, firstName, lastName, birthDate, phone, email, commissionRate, grossSales, basicSalary);
                    employeeList.Add(salariedEmployee);
                }
                else if (choice == 2)
                {
                    string ssn = AutoIncrementSSN("HE");
                    Console.Write("Enter Wage: ");
                    double wage = Validate.ValidateNumber("Wage");
                    Console.Write("Enter Wrking Hour: ");
                    double workingHour = Validate.ValidateNumber("Working Hour");
                    HourlyEmployee hourlyEmployee = new HourlyEmployee(ssn, firstName, lastName, birthDate, phone, email, wage, workingHour);
                    employeeList.Add(hourlyEmployee);

                }
                Console.WriteLine("Do you want to continue importing employee? \n(Press 'Y' for continue or 'N' for stop!)");
                string check = Validate.ValidateContinueChoice("Y", "N");

                if (check.Equals("Y")||check.Equals("y"))
                {
                    ImportEmployee();
                }
            }
        }
        public string AutoIncrementSSN(string type)
        {
            int numberOfEmployee = employeeList.Count;
            string ssn = type + 1;
            if (numberOfEmployee > 0)
            {
                string number = employeeList[numberOfEmployee - 1].ssn.Substring(2);
                ssn = type + (int.Parse(number) + 1);
            }
            return ssn;
        }
        public void DisplayEmployee()
        {
            Console.WriteLine("========= Display Employee =========");
            Console.WriteLine("1. Salaried Employee.");
            Console.WriteLine("2. Hourly Employee.");
            Console.WriteLine("3. All Employee.");
            Console.WriteLine("4. Main Menu");
            Console.Write("Enter Menu Option Number: ");
            int choice = Validate.ValidateChoice(1, 4);
            if (choice == 1 || choice == 2 || choice == 3)
            {
                if(choice == 1)
                {
                    DisplayEmployeeByCondition("Salaried", null);
                }    
                else if(choice == 2)
                {
                    DisplayEmployeeByCondition("Hourly", null);
                }
                else
                {
                    DisplayEmployeeByCondition("Salaried", null);
                    Console.WriteLine();
                    DisplayEmployeeByCondition("Hourly", null);
                }
                Console.Write("Do you want to continue display employee? (Press 'Y' for continue or 'N' for stop!");
                string check = Validate.ValidateContinueChoice("Y", "N");

                if (check.Equals("Y") || check.Equals("y"))
                {
                    DisplayEmployee();
                }
            }
        }
        public void DisplayEmployeeByCondition(string type, string name)
        {
            List<HourlyEmployee> hourlyEmployees = new List<HourlyEmployee>();
            List<SalariedEmployee> salariedEmployees = new List<SalariedEmployee>();
            foreach (Employee employee in employeeList)
            {
                if (employee is HourlyEmployee)
                {
                    hourlyEmployees.Add((HourlyEmployee)employee);
                }
                if (employee is SalariedEmployee)
                {
                    salariedEmployees.Add((SalariedEmployee)employee);
                }
            }

            if (type == "Salaried" || type == "salaried")
            {
                
                if (salariedEmployees.Count > 0)
                {
                    Console.WriteLine("List Of "+ type+" Employee: ");
                    Console.WriteLine(string.Format("{0, -5} {1, -12} {2, -10} {3, -12} {4, -12} {5, -15} {6, -10} {7, -10} {8, -10}",
                          "SSN", "FirstName", "LastName", "BirthDate", "Phone", "Email", "Commission Rate", "Gross Sales", "Basic Salary"));
                    if (name == null)
                    {
                        foreach (SalariedEmployee employee in salariedEmployees)
                        {
                            Console.WriteLine(employee.ToString());
                        }
                    }
                    if (name != null)
                    {
                        bool found = false;
                        foreach (SalariedEmployee employee in salariedEmployees)
                        {
                            if (employee is SalariedEmployee && (employee.firstName.Equals(name) || employee.firstName.ToLower().Equals(name) || employee.lastName.Equals(name) || employee.lastName.ToLower().Equals(name)))
                            {
                                Console.WriteLine(employee.ToString());
                                found = true;
                            }
                        }
                        if (!found)
                        {
                            Console.WriteLine("There is no "+ type + " employee having name " + name);
                        }
                    }
                }
                else
                    Console.WriteLine("There is no " + type + " employee!");
            }
            else if (type == "Hourly" || type == "hourly")
            {        
                if (hourlyEmployees.Count > 0)
                {
                    Console.WriteLine("List of " + type + " employee: ");
                    Console.WriteLine(string.Format("{0, -5} {1, -12} {2, -10} {3, -12} {4, -12} {5, -15} {6, -7} {7, -10} ",
                          "SSN", "FirstName", "LastName", "BirthDate", "Phone", "Email", "Wage", "Working Hours"));
                    if (name == null)
                    {
                        foreach (HourlyEmployee employee in hourlyEmployees)
                        {
                            Console.WriteLine(employee.ToString());
                        }
                    }
                    if (name != null)
                    {
                        bool found = false;
                        foreach (HourlyEmployee employee in hourlyEmployees)
                        {
                            if (employee is HourlyEmployee && (employee.firstName.Equals(name) || employee.firstName.ToLower().Equals(name) || employee.lastName.Equals(name) || employee.lastName.ToLower().Equals(name)))
                            {
                                Console.WriteLine(employee.ToString());
                                found = true;
                            }
                        }
                        if (!found)
                        {
                            Console.WriteLine("There is no " + type + " employee having name " + name);
                        }
                    }
                }
                else
                    Console.WriteLine("There is no " + type + " employee!");
            }

        }
        public void SearchEmployee()
        {
            Console.WriteLine("========= Search Employee =========");
            Console.WriteLine("1. By Employee Type.");
            Console.WriteLine("2. By Employee Name.");
            Console.WriteLine("3. Main Menu.");
            Console.Write("Enter Menu Option Number: ");
            int choice = Validate.ValidateChoice(1, 3);
            if(choice ==1 || choice ==2)
            {
                if (choice == 1)
                {
                    Console.Write("Enter Employee Type: ");
                    string type = Validate.ValidateContinueChoice("Hourly", "Salaried");
                    if (type.Equals("Hourly") || type.Equals("hourly"))
                    {
                        DisplayEmployeeByCondition("Hourly", null);
                    }
                    else if (type.Equals("Salaried") || type.Equals("salaried"))
                    {
                        DisplayEmployeeByCondition("Salaried", null);
                    }
                }
                if (choice == 2)
                {
                    Console.Write("Enter Employee Name: ");
                    string name = Validate.ValidateName();
                    DisplayEmployeeByCondition("Hourly", name);
                    Console.WriteLine();
                    DisplayEmployeeByCondition("Salaried", name);
                }
                Console.Write("Do you want to continue search employee? (Press 'Y' for continue or 'N' for stop!");
                string check = Validate.ValidateContinueChoice("Y", "N");

                if (check.Equals("Y") || check.Equals("y"))
                {
                    SearchEmployee();
                }
            }
               
        }
    }
}
