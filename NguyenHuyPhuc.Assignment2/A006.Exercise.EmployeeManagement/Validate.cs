﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace A006.Exercise.EmployeeManagement
{
    internal class Validate
    {
        //Hàm ValidateChoice dùng để ép người dùng nhập vào lựa chọn là kiểu số nguyên trong phạm vi yêu cầu
        public static int ValidateChoice(int min, int max)
        {
            while (true)
            {
                string StringInput = Console.ReadLine();
                try
                {
                    if (string.IsNullOrEmpty(StringInput))
                    {
                        Console.Write("The option must not be empty!\nEnter a number again: ");
                    }
                    else
                    {
                        int IntegerInput = int.Parse(StringInput);
                        if (IntegerInput < min || IntegerInput > max)
                        {
                            Console.WriteLine("Invalid number! Please enter an number from " + min + " to " + max);
                            Console.Write("Enter a number from " + min + " to " + max + ": ");
                        }
                        else return IntegerInput;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid number! Please enter an number from " + min + " to " + max);
                    Console.Write("Enter a number from " + min + " to " + max + ": ");
                }
            }
        }

        //Hàm ValidateBirthDate dùng để ép người dùng nhập vào ngày sinh có dạng dd/MM/yyyy
        public static DateTime ValidateBirthDate()
        {
            while (true)
            {
                try
                {
                    string input = Console.ReadLine();
                    DateTime inputDate = DateTime.ParseExact(input, "dd/MM/yyyy", null);
                    return inputDate;
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid Datetime! Please enter birthday format (dd/MM/yyyy)! ");
                    Console.Write("Enter birthday format (dd/MM/yyyy): ");
                }

            }

        }

        //Hàm ValidatePhoneNumber dùng để ép người dùng nhập vào số điện thoại có ít nhất 7 số nguyên dương
        public static string ValidatePhoneNumber()
        {
            string pattern = @"[0-9]{7,}";
            while (true)
            {
                string phone = Console.ReadLine().ToString();
                if (Regex.IsMatch(phone, pattern))
                {
                    return phone;
                }
                else
                {
                    Console.WriteLine("Invalid Phone Number! Please Enter phone number again (Minimum 7 positive integer)!");
                    Console.Write("Enter phone number: ");

                }
            }
        }

        //Hàm IsEmail dùng để kiểm tra xem email nhập vào đúng format không
        public static bool IsEmail(string email)
        {
            string[] part = email.Split('@');
            if (part.Length != 2)
            {
                return false;
            }

            string localPart = part[0];
            string domainPart = part[1];

            //Check format local part
            if (localPart.Length == 0 || localPart[0] == '.' || localPart[localPart.Length - 1] == '.')
            {
                return false;
            }

            string patternLocal = @"^[\w!#$%&'*+\-\/=?^_{}|~]*(\.[\w!#$%&'*+\-\/=?^_{}|~]*)?$";
            if (!Regex.IsMatch(localPart, patternLocal))
            {
                return false;
            }

            // Check format domain part
            if (domainPart.Length == 0 || domainPart[0] == '-' || domainPart[domainPart.Length - 1] == '-')
            {
                return false;
            }

            string patternDomain = @"^[\w-]*(\.[\w-]*)*$";
            if (!Regex.IsMatch(domainPart, patternDomain))
            {
                return false;
            }

            // Check if the top-level domain is all-numeric or not 
            string[] domainParts = domainPart.Split('.');
            if (domainParts.Length >= 1 && domainParts[domainParts.Length - 1].All(char.IsDigit))
            {
                return false;
            }

            return true;
        }

        //Hàm ValidateEmail ép người dùng nhập vào email đúng định dạng
        public static string ValidateEmail()
        {
            while (true)
            {
                string email = Console.ReadLine().ToString();
                bool a = IsEmail(email);
                if (a)
                {
                    return email;
                }
                else
                {
                    Console.WriteLine("Invalid Email! Please enter email again!");
                    Console.Write("Enter email: ");
                }
            }
        }
        //Hàm ValidateContinueChoice ép người dùng nhập vào string a hay b
        public static string ValidateContinueChoice(string a, string b)
        {
            while (true)
            {
                string choice = Console.ReadLine().ToString();
                if (choice == a || choice == b || choice == a.ToLower() || choice == b.ToLower())
                {
                    return choice;
                }
                else
                {
                    Console.WriteLine("Invalid Choice!Please enter again");
                    Console.Write("Enter your option ( " + a + " or " + b + " ): ");
                }
            }
        }
        //Hàm ValidateName ép người dùng nhập vào chữ cái
        public static string ValidateName()
        {
            string pattern = @"^[A-Za-z\s]+$";
            while (true)
            {
                string name = Console.ReadLine().ToString();
                if (Regex.IsMatch(name, pattern))
                {
                    return name;
                }
                else
                {
                    Console.WriteLine("Invalid Name! Please Enter only characters!");
                    Console.Write("Enter again: ");
                }
            }
        }
        //Hàm ValidateNumber ép người dùng nhập vào số lớn hơn 0
        public static double ValidateNumber(string character)
        {
            while (true)
            {
                string StringInput = Console.ReadLine();
                try
                {
                    if (string.IsNullOrEmpty(StringInput))
                    {
                        Console.Write("The " + character + " must not be empty!\nEnter a number again: ");
                    }
                    else
                    {
                        double DoubleInput = double.Parse(StringInput);
                        if (DoubleInput < 0)
                        {
                            Console.WriteLine("Invalid number! The " + character + " must be equal or greater than 0!");
                            Console.Write("Enter a number again: ");
                        }
                        else return DoubleInput;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid number! The " + character + " must be equal or greater than 0!");
                    Console.Write("Enter a number again: ");
                }
            }
        }
    }
}
