﻿using System.Numerics;
using System.Runtime.Intrinsics.X86;

namespace A006.Exercise.EmployeeManagement
{

    internal class Employee
    {
        public string ssn { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public DateTime birthDate { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public Employee() { }

        public Employee(
            string ssn,
            string firstName,
            string lastName,
            DateTime birthDate, 
            string phone, 
            string email)
        {
            this.ssn = ssn;
            this.firstName = firstName;
            this.lastName = lastName;
            this.birthDate = birthDate;
            this.phone = phone;
            this.email = email;
        }

        public override string? ToString()
        {
            return string.Format("{0, -5} {1, -12} {2, -10} {3, -12} {4, -12} {5, -15}", ssn, firstName, lastName, birthDate, phone, email);
        }
             
    }
}


